## Revision
### 2020.08.01
* first commit

# Client Application/Kiosk와 Pocavision API 연동 규격
### Application/Kiosk와 Pocavision API Sequence diagram

![Alt text](./images/kiosk_pocavision_api.png "Application/Kiosk와 Pocavision API Sequence diagram")

### Pocavision API 접속 URL
* https://api.roborus.io (debug mode)
### API 테스트 실행 툴
* swagger: https://api.roborus.io/swagger
* postman: https://www.postman.com

### Pocavision API 호출 방법
**1. 먼저 토근을 얻는다. (Application -> Server)**

* request

| API              | Body (JSON)        | Type  |
| ---------------- |:------------------:| ------:|
| /api/token/auth/ | username(string)   | 발급된 사용자 이름을 입력한다. |
|                  | password(string)   | 발급된 사용자 암호를 입력한다. |
|

* 예시
```bash
POST "https://api.roborus.io/api/token/auth/
```
```json
{
    "username": "username",
    "password": "password"
}
```

* response

| code             |  response (JSON) | 설명 |
| ---------------- |:------------------:|  ------:|
| 200  | token:{token}   | 계정이 있으면 token이 발급된다. |
| 400  | Bad Request     | 계정이 없거나 잘못된 포멧이면 요청이 거부된다. |
|

* 예시
```json
{
    "token": "DrNJdXrzTSPuREkJGyhmzolIrRy7bmNQOTX5Z5t189rwkTjNWkuwWcKMnbGVFibZ"
}
```

**2. 발급된 토큰으로 image와 id_number를 입력하여 /predict/photo/ 호출한다. (Application -> Server)**

* request

| API              | Body (JSON)        | Type  |
| ---------------- |:------------------:| ------:|
| /api/predict/photo/  | image(file)   | 640x480 정도 Quality의 jpg/png 이미지를 입력한다. |
|                  | id_number(number)   | 사용자의 고유 번호를 입력한다. |
|

* 예시
```bash
POST "https://api.roborus.io/api/predict/photo/
```
```json
{
    "image": "{...}",
    "id_number": "14132"
}
```

* response

| code         |  response (JSON) | 설명 |
| ---------------- |:------------------:|  ------:|
| 200  | kicket(string)   | 정상적인 사진이면 kicket을 발급한다. |
|      | status(string)   | task의 상태를 리턴한다. "SUCCESS" 또는 "PENDING"|
|      | result(string)   | task의 상태가 "SUCCESS" 이면 결과값을 리턴한다. |
| 201  | no face(string)   | 사진속에 얼굴이 없으면 no face라고 응답한다. 이경우 앱은 **재호출** 여부를 판단해야한다. |
| 202  | low quality(string)   | 사진속의 얼굴이 정면으로 찍혔는지에 대한 확률이 90% 이하이면 리턴한다. 이경우 앱은 **재호출** 여부를 판단해야한다.|
|   | score(number)   | 90% 이하의 확률값 |
| 400  | Bad Request     | 잘못된 이미지거나 포멧이면 요청이 거부된다. |
|

* 예시
```json
{
    "ticket": "c4617248-d6f1-4a47-b8b5-ced143042ada",
    "status": "PENDING",
    "result": null
}
```

**3. ticket을 받고 /predict/result/ticket를 호출하여 vector data를 얻어온다. (Application -> Server)**

* request

| API                   | Body (JSON)        | Type  |
| --------------------- |:------------------:| ------:|
| /api/predict/result/kicket/  | kicket(string)   | 받은 티켓을 바로 호출하여 결과를 얻어 온다. |
|

* 예시
```bash
GET "https://api.roborus.io/api/result/123e4567-e89b-12d3-a456-426614174000/
```

* response

| code         |  response (JSON) | 설명 |
| ---------------- |:------------------:|  ------:|
| 200  | task_state(string)   | 발부된 ticket의 task의 상태를 얻어온다. "PENDING"이라면 **재 호출야한다** |
|      | status(string)   | task의 상태를 리턴한다. "SUCCESS" 또는 "PENDING"|
|      | result(string)   | task의 상태가 "SUCCESS" 이면 feature, score, ticket을 리턴한다. |
| 201  | not found(string)   | 받은 ticket의 task가 없을경우 |
| 400  | Bad Request     | 잘못된 티켓이거나 포멧이면 요청이 거부된다. |
|

* 예시
```json
{
    "ticket": "c4617248-d6f1-4a47-b8b5-ced143042ada",
    "status": "SUCCESS",
    "result": {
        "contents": "{\"feature\":\"QpwAAAAAAAAMAgAATEcXPVXKh71wl7s9VoYsPdxCBb6DQWG9S5m2vfEGdr3M7xW9Opvxur3QTD2Fu8u9oq8DvpZWLz6zI5W97Tq1O4FINj0NdRA99nJpOrG4hb24PcI9cjXRPaGMdbsHje872zNDvNX8Pr1eDIY9p/enveb727zFVCo6YhDHPHhqFz2gen89ClZovt4hQDzH6rc8NZ1hur69pD1oezS+tW8iPT9pFr2v1q+8wb/AvE7iXLz0lCY82igzPeJq6j0vhjs8JBgqvp9DAD4dgXU9b/GHPPuZY72k8ko9ZEGyvVLGnT3QnPg9lglUPBHdrD2CgUG9zyHRvFzcO73WT+87j9zcvV4m6L2rQwq+LZsZPku3173efHy7LnBuvaGBEL2Rau+6fofvvdYD9L2AT5A9gI19PEIfjz2tUxE98+KXvT2BND6KdHi+togNvqmxgD3PpSu+FLFCvBbxBr5eCue9WrGfvXyqRT37gy8+AYImPfLaxb1RsjK+PgxEveLG/r2hfzI8ueFyPTYTajw6rwo8HJmEPCPx1z3WlBS+rpwdvtCkBz2zkca9dgizvEUjzz0j/dC8HRRTvR/Or724yqC9Y45EvVh4Mr4g4WQ8Q4v/vC22oD02v6y9eWWVPbwzUj6qVH27IpRnvXjgOb2W+Zu9KaPFvRnunj2oW8i9z8nyPLpJ+j0=\",\"score\":\"0.9\",\"ticket\":\"c4617248-d6f1-4a47-b8b5-ced143042ada\"}",
        "title": "it is done!"
    }
}
```

**4. 발급된 토큰으로 사진을 첨부하여 /identity/photo/ 호출한다. (Server <- Kiosk>)**

* request

| API              | Body (JSON)        | Type  |
| ---------------- |:------------------:| ------:|
| /api/identity/photo/  | image(file)   | 640x480 정도의 퀄리티의 jpg/png 이미지를 입력한다. |
|

* 예시
```bash
POST "https://api.roborus.io/api/identity/photo/
```
```json
{
    "image": "{...}"
}
```

* response

| code  |  response (JSON)   | 설명 |
| -------------------------- |:------------------:|  ------:|
| 200   | kicket(string)      | 정상적인 사진이면 kicket을 발급한다. |
|       | status(string)   | task의 상태를 리턴한다. "SUCCESS" 또는 "PENDING"|
|       | result(string)   | task의 상태가 "SUCCESS" 이면 결과값을 리턴한다. |
| 201   | no face(string)     | 사진속에 얼굴이 없으면 no face라고 응답한다. 이경우 kiosk는 **재호출** 여부를 판단해야한다. |
| 202   | low quality(string) | 사진속의 얼굴이 정면으로 찍혔는지에 대한 확률이 90% 이하이면 리턴한다. 이경우 kiosk는 **재호출** 여부를 판단해야한다.|
|       | score(number)       | 90% 이하의 확률값 |
| 400   | Bad Request         | 잘못된 이미지거나 포멧이면 요청이 거부된다. |
|

* 예시
```json
{
    "ticket": "123e4567-e89b-12d3-a456-426614174000",
    "status": "PENDING",
    "result": null
}
```

**5. ticket을 받고 /identity/result/ticket를 호출하여 vector data를 얻어온다. (Server <- Kiosk)**

* request

| API                           | Body (JSON)      | Type  |
| ----------------------------- |:----------------:| ------:|
| /api/identity/result/kicket/  | kicket(string)   | 받은 티켓을 바로 호출하여 결과를 얻어 온다. |
|

* 예시
```bash
GET "https://api.roborus.io/api/result/123e4567-e89b-12d3-a456-426614174000/
```

* response

| code         |  response (JSON) | 설명 |
| ---------------- |:------------------:|  ------:|
| 200  | task_state(string)  | 발부된 ticket의 task의 상태를 얻어온다. "PENDING"이라면 **재 호출야한다** |
|      | status(string)   | task의 상태를 리턴한다. "SUCCESS" 또는 "PENDING"|
|      | result(string)   | task의 상태가 "SUCCESS" 이면 found/not found, ticket을 리턴한다. |
| 201  | not found(string)   | 받은 ticket의 task가 없을경우 |
| 400  | Bad Request         | 잘못된 티켓이거나 포멧이면 요청이 거부된다. |
|

* 예시
```json
{
    "ticket": "123e4567-e89b-12d3-a456-426614174000",
    "status": "SUCCESS",
    "result": {
        "contents": "{\"result\":\"found\",\"ticket\":\"c4617248-d6f1-4a47-b8b5-ced143042ada\"}",
        "title": "it is done!"
    }
}
```

**6. 위의 결과 값이 실패라면 qrcode의 저장 데이터(features,id_number)를 서버로 전송한다. (Server <- Kiosk)**

* request

| API                   | Body (JSON)        | Type  |
| --------------------- |:------------------:| ------:|
| /api/identity/qrcode/ | features(string)   | qrcode에서 추출된 features를 입력한다. |
|                       | id_number(number)  | qrcode에서 추출된 id_number를 입력한다. |
|

* 예시
```bash
POST "https://api.roborus.io/api/identity/qrcode/
```
```json
{
    "features": "DrNJdXrzTSPuREkJGyhmzolIrRy7bmNQOTX5Z5t189rwkTjNWkuwWcKMnbGVFibZ...",
    "id_number": "14132"
}
```

* response

| code         |  response (JSON) | 설명 |
| ---------------- |:------------------:|  ------:|
| 200  | found(string)   | features와 id_number가 일치하면 found 리턴. |
| 201  | not found(string)   | 일치하지 않으면 not found를 리턴한다. |
| 400  | Bad Request     | 잘못된 티켓이거나 포멧이면 요청이 거부된다. |
|


* 예시
```json
{
    "task_state": "SUCCESS",
    "task_result": "found"
}
```